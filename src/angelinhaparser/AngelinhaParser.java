package angelinhaparser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

public class AngelinhaParser {
	private File input_file, output_file;
	private Scanner sc;
	private BufferedWriter writer;
	private String event_name;
	private int num_of_entries = 0;
	
	public AngelinhaParser(File input_file, String event_name, File output_file) {
		this.input_file = input_file;	
		this.event_name = event_name;
		this.output_file = output_file;
	}

	public static void main(String[] args) throws Exception {
		if (args.length > 0 && args.length < 4) {
			File file = new File(args[0]);
			if (file.canRead()) {
				String event = (args.length > 1)? args[1] : "anti_proton";
				File output_file = null;
				if (args.length > 2) output_file = new File(args[2]);
				AngelinhaParser parser = new AngelinhaParser(file, event, output_file);
				parser.parse();
				System.exit(0);
			} else {
				System.err.println("The specified file path does not exist or it cannot be read.");
				printHelp();
			}
		} else printHelp();
		System.exit(1);
	}
	
	public static void printHelp() {
		System.out.println("SYNOPSIS \n\tangelinhaparser SOURCE [EVENT] [DEST]");
	}
	
	private Writer getWriter() throws Exception {
		if (output_file != null) {
			if (!output_file.exists()) {
				//output_file.mkdirs();
				output_file.createNewFile();
			}
			if (output_file.canWrite()) return new PrintWriter(output_file);
			else {
				System.err.println("Can't write to specified output file. Aborting...");
				System.exit(1);
			}
		}
		return new OutputStreamWriter(System.out);
	}
	
	public void parse() throws Exception {
		sc = new Scanner(input_file);
		writer = new BufferedWriter(getWriter());
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (line.startsWith("* G4Track Information:   Particle = " + event_name))	parseEntries();
		}
		writer.close();
		System.err.println("Parsing finished: wrote " + num_of_entries + " entries.");
	}
	
	private void parseEntries() throws Exception {
		++num_of_entries;
		for (int c = 0; c < 3 && sc.hasNextLine(); ++c) sc.nextLine();
		while (sc.hasNextLine()) {
			if (sc.hasNextInt()) parseStep();
			else {
				String line = sc.nextLine();
				if (line.startsWith("*********************************************************************************************************")) 
					return;
			}
		}
	}
	
	private void parseStep() throws Exception {
		String step_line = sc.nextLine();
		String[] tokens = step_line.trim().split("\\s+");
		if (tokens.length == 10) writeStep(step_line, tokens);
		else System.err.println("Line does not match expected number of tokens: " + step_line);
	}
	
	private void writeStep(String line, String[] tokens) throws Exception {
		writer.write("\t" + num_of_entries + line + "\n");
	}
}
