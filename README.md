# angelinhaparser

To compile and run:
From within the cloned git repo root directory:
```
mkdir bin
mkdir bin/angelinhaparser
javac -d ./bin src/angelinhaparser/AngelinhaParser.java
java -cp ./bin angelinhaparser.AngelinhaParser <source_data> [event_name] [output_file]
```
There is also in the root folder an angelinhaparser bash script that can be edited to fix the paths (e.g. make them absolute) then it can be copied to the user binaries folder to use directly instead of callint java with parameters.